json.array!(@users) do |user|
  json.extract! user, :id, :fname, :lname, :time_zone, :roles_mask, :provider, :uid
  json.url user_url(user, format: :json)
end
