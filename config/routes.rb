Rails.application.routes.draw do
  # authenticated :user do
  #   root :to => 'redirections#index', :as => :authenticated_root
  # end

  root :to => redirect('/users/sign_in')

  devise_for :users, :controllers => { :omniauth_callbacks => 'users/omniauth_callbacks' }

  resource :user, only: [:show]

  namespace :admin do
    get '/' => 'admin#index'
    resources :users
  end
end
